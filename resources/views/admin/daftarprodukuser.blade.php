@extends('admin/template')

@section('isianadmin')

<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Daftar Produk</h2>
          <ol>
            <li><a href="{{ url('/') }}">Home</a></li>
            <li>Daftar Produk</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Portfolio Section ======= -->
    <section id="portfolio" class="portfolio">
      <div class="container">
      @if(session('pesansistem'))
      <div class="alert alert-warning alert-dismissible fade show" role="alert">
          {{ session('pesansistem') }}

      </div>

      @endif
      <br><br>
        <div class="row portfolio-container" data-aos="fade-up">
          @foreach ($produk as $prd)
          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <img src="{{ asset('gambar/'.$prd->foto) }}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>{{ $prd->nama }}</h4>
              <?php
                $datanya = $prd->ket;
                $rekap = substr($datanya,0,50);
              ?>
              <p><?= $rekap; ?></p>
              <a href="{{ asset('gambar/'.$prd->foto) }}" data-gall="portfolioGallery" class="venobox preview-link" title="{{ $prd->nama }}"><i class="bx bx-plus"></i></a>
              <a href="/dafproduk/detail/{{$prd->id}}" class="details-link" title="More Details"><i class="bx bx-search"></i></a>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </section><!-- End Portfolio Section -->

  </main><!-- End #main -->
<br><br><br><br><br><br><br>

@endsection
