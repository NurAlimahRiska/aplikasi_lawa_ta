@extends('admin/template')

@section('isianadmin')
    
<main id="main">

  <!-- ======= Breadcrumbs ======= -->
  <section id="breadcrumbs" class="breadcrumbs">
    <div class="container">

      <div class="d-flex justify-content-between align-items-center">
        <h2>Tentang Kami</h2>
        <ol>
          <li><a href="{{ url('/') }}">Beranda</a></li>
          <li>Tentang Kami</li>
        </ol>
      </div>

    </div>
  </section><!-- End Breadcrumbs -->


<!-- ======= Our Team Section ======= -->
<section id="team" class="team section-bg">
  <div class="container">
    <div class="section-title" data-aos="fade-up">
      <h2> <strong>{{ $isikonten->judul }}</strong></h2>
      <p>{{ $isikonten->isi }}</p>
    </div>
  </div>
</section><!-- End Our Team Section -->

</main><!-- End #main -->

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>


@endsection