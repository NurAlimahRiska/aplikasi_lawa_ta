@extends('admin/template')

@section('isianadmin')

<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Daftar Produk</h2>
          <ol>
            <li><a href="{{ url('/') }}">Home</a></li>
            <li>Daftar Produk</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Portfolio Section ======= -->
    <section id="portfolio" class="portfolio">
      <div class="container">

      <a href="/daftarproduk/tambah" class="btn btn-primary">TAMBAH DATA</a><br>
      @if(session('pesansistem'))
      <div class="alert alert-warning alert-dismissible fade show" role="alert">
          {{ session('pesansistem') }}

      </div>

      @endif
      <br><br>
      <table class="table">
            <thead class="thead-dark">
              <tr>
                <th scoppe="col" width="8%">NO</th>
                <th scoppe="col" width="32%">FOTO</th>
                <th scoppe="col" width="17%">KODE</th>
                <th scoppe="col" width="25%">NAMA</th>
                <th scoppe="col" width="18%">OPSI</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($produk as $prd)
              <tr>
                <th scope="row">{{ $loop->iteration }}</th>
                <td><img src="{{ asset('gambar/'.$prd->foto) }}" width="20%" alt=""></td>
                <td>{{ $prd->kode }}</td>
                <td>{{ $prd->nama }}</td>

                <td>
                  <a href="/daftarproduk/detail/{{$prd->id}}" class="badge badge-info" title="Detail">Detail</a>
                  <a href="/daftarproduk/edit/{{$prd->id}}" class="badge badge-warning">Edit</a>
                  <button type="button" class="badge badge-danger" data-toggle="modal" data-target="#modalSaya{{ $prd->id }}">
                    Delete
                  </button>

                  <!-- Contoh Modal -->
                  <div class="modal fade" id="modalSaya{{ $prd->id }}" tabindex="-1" role="dialog" aria-labelledby="modalSayaLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="modalSayaLabel">Hapus <b>{{ $prd->nama }}</b></h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          Yakin akan menghapus produk <strong>{{ $prd->nama }}</strong>, data yang dihapus tidak dapat di kembalikan.. lanjutkan ?
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                          <a href="/daftarproduk/delete/{{ $prd->id }}" class="btn btn-primary">Oke</a>
                        </div>
                      </div>
                    </div>
                  </div>
                <!-- utup modal -->

                </td>
              </tr>
            @endforeach
            </tbody>
          </table>




        <div class="row portfolio-container" data-aos="fade-up">



          <!-- @foreach ($produk as $prd)
          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <img src="{{ asset('gambar/'.$prd->foto) }}" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>{{ $prd->nama }}</h4>
              <p>{{ $prd->ket }}</p>
              <a href="{{ asset('gambar/'.$prd->foto) }}" data-gall="portfolioGallery" class="venobox preview-link" title="{{ $prd->nama }}"><i class="bx bx-plus"></i></a>
              <a href="/daftarproduk/detail/{{$prd->id}}" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>
          @endforeach -->
        </div>
      </div>
    </section><!-- End Portfolio Section -->

  </main><!-- End #main -->
<br><br><br><br><br><br><br>

@endsection
