@extends('admin/template')

@section('isianadmin')

<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Tambah Produk</h2>
          <ol>
            <li><a href="{{ url('/') }}">Home</a></li>
            <li><a href="{{ url('/dafproduk') }}">Daftar Produk</a></li>
            <li>Pembelian Produk</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Portfolio Details Section ======= -->
    <section id="portfolio-details" class="portfolio-details">
      <div class="container">
        <div class="row">
            <form action="/dafproduk/insert" method="post" enctype="multipart/form-data">
            @csrf
                <div class="mb-3">
                <?php
                    date_default_timezone_set('Asia/Jakarta');
                    $tanggalsekarang = date("Y-m-d h:i:s");
                ?>
                    <label for="kodeproduk" class="form-label">Kode Produk</label>
                    <input value="{{ $produk->kode }}" type="text" name="kodeproduk" class="form-control" id="kodeproduk" readonly>
                    <input type="hidden" name="tanggalsekarang" value="<?= $tanggalsekarang; ?>">
                </div>
                <div class="mb-3">
                    <label for="namaproduk" class="form-label">Nama Produk</label>
                    <input type="text" value="{{ $produk->nama }}" name="namaproduk" class="form-control" id="namaproduk" readonly>
                </div>
                <div class="mb-3">
                    <label for="hargaproduk" class="form-label">Harga Produk</label>
                    <input type="text" value="{{ $produk->harga }}" name="hargaproduk" class="form-control" id="hargaproduk" readonly>
                </div>
                <div class="mb-3">
                    <label for="fotoproduk" class="form-label">Produk</label>
                    <input type="hidden" value="{{ $produk->foto }}" name="fotoproduk" id="fotoproduk">
                    <img src="{{ asset('gambar') }}/{{ $produk->foto }}" width="10%" alt="">
                </div>
                <div class="mb-3">
                    <label for="jumlahproduk" class="form-label">jumlah Pembelian</label>
                    <select name="jumlahproduk" id="jumlahproduk" class="form-control">
                        @for($x=1 ; $x<=500 ; $x++)
                            <option value="<?= $x; ?>"><?= $x; ?></option>
                        @endfor
                    </select>
                </div>
                <div class="mb-3">
                    <label for="namapenerima" class="form-label">Nama Penerima</label>
                    <input type="text" value="{{ old('namapenerima') }}" name="namapenerima" class="form-control @error('namapenerima') is-invalid @enderror" id="namapenerima">
                    @error('namapenerima')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="telppenerima" class="form-label">Telp Penerima</label>
                    <input type="text" value="{{ old('telppenerima') }}" name="telppenerima" class="form-control @error('telppenerima') is-invalid @enderror" id="telppenerima">
                    @error('telppenerima')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="alamatpenerima" class="form-label">Alamat Penerima</label>
                    <input type="text" value="{{ old('alamatpenerima') }}" name="alamatpenerima" class="form-control @error('alamatpenerima') is-invalid @enderror" id="alamatpenerima">
                    @error('alamatpenerima')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Simpan</button>
                </div>
            </form>
        </div>
      </div>
    </section><!-- End Portfolio Details Section -->

  </main><!-- End #main -->
<br><br><br><br><br><br><br>

@endsection
