@extends('admin/template')

@section('isianadmin')

<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Detail Transaksi</h2>
          <ol>
            <li><a href="{{ url('/') }}">Home</a></li>
            <li><a href="{{ url('/dafartransaksi') }}">Daftar Transaksi</a></li>
            <li>Detail Transaksi</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Portfolio Details Section ======= -->
    <section id="portfolio-details" class="portfolio-details">
      <div class="container">
        <h2 class="portfolio-title">{{ $produk->nama }}</h2>
        <div class="row">

          <div class="col-lg-8" data-aos="fade-right">
            <div class="owl-carousel portfolio-details-carousel">
              <img src="{{ asset('gambar/'.$produk->foto)}}" class="img-fluid" alt="">
            </div>
          </div>

          <div class="col-lg-4 portfolio-info" data-aos="fade-left">
            <h3>Detail Transaksi</h3>
            <ul>
              <li><strong>Nama Produk</strong>: {{$produk->nama}}</li>
              <li><strong>Kode Barang</strong>: {{$produk->kode}}</li>
              <li><strong>Jumlah Barang</strong>: {{$produk->jumlah}}</li>
              <li><strong>Harga Barang</strong>: {{$produk->harga}}</li>
              <li><strong>Nama Penerima</strong>: {{$produk->namapenerima}}</li>
              <li><strong>Telp Penerima</strong>: {{$produk->telppenerima}}</li>
              <li><strong>AlamatPenerima</strong>: {{$produk->alamatpenerima}}</li>
              <li><strong>Total Belanja</strong>: {{$produk->total}}</li>
              <li><strong>Tanggal Pemesanan</strong>: {{$produk->tanggal}}</li>
              <li>Bukti Pembayaran : </li>
              @if($produk->ket == "Belum Upload Bukti Bayar")
                <strong><b>Belum ada bukti pembayaran</b></strong>
              @else
                <img src="{{ asset('gambar/'.$produk->bukti)}}" class="img-fluid" alt="">
              @endif
              <li></li>


              <!-- <li><strong>Project URL</strong>: <a href="#">www.example.com</a></li> -->
            </ul>
            <a href="/daftartransaksi" class="btn btn-primary">Kembali</a>&nbsp;&nbsp;&nbsp;
          </div>

        </div>

      </div>
    </section><!-- End Portfolio Details Section -->

  </main><!-- End #main -->
<br><br><br><br><br><br><br>

@endsection
