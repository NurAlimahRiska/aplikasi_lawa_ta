<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>SELAMAT DATANG {{ Auth::user()->name }}</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{ asset('desain') }}/assets/img/favicon.png" rel="icon">
  <link href="{{ asset('desain') }}/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Muli:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('desain') }}/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{ asset('desain') }}/assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="{{ asset('desain') }}/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="{{ asset('desain') }}/assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="{{ asset('desain') }}/assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="{{ asset('desain') }}/assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="{{ asset('desain') }}/assets/vendor/aos/aos.css" rel="stylesheet">
  <!-- modal -->
  <link rel="stylesheet" href="{{ asset('modal') }}/assets/css/bootstrap.css">



  <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous"> -->
  <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script> -->

  <!-- Template Main CSS File -->
  <link href="{{ asset('desain') }}/assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Flattern - v2.0.1
  * Template URL: https://bootstrapmade.com/flattern-multipurpose-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Top Bar ======= -->
  {{-- <section id="topbar" class="d-none d-lg-block">
    <div class="container d-flex">
      <div class="contact-info mr-auto">
        <i class="icofont-envelope"></i><a href="mailto:contact@example.com">contact@example.com</a>
        <i class="icofont-phone"></i> +1 5589 55488 55
      </div>
      <div class="social-links">
        <a href="#" class="twitter"><i class="icofont-twitter"></i></a>
        <a href="#" class="facebook"><i class="icofont-facebook"></i></a>
        <a href="#" class="instagram"><i class="icofont-instagram"></i></a>
        <a href="#" class="skype"><i class="icofont-skype"></i></a>
        <a href="#" class="linkedin"><i class="icofont-linkedin"></i></i></a>
      </div>
    </div>
  </section> --}}

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container d-flex">

      <div class="logo mr-auto">
        <h1 class="text-light"><a href="{{ url('/') }}">Nama Toko</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="{{ request()->is('/') ? 'active' : '' }}"><a href="{{ url('/') }}">Beranda</a></li>
          <li class="{{ request()->is('tentang') ? 'active' : '' }}"><a href="{{ url('/tentang') }}">Tentang Kami</a></li>
        @if(auth()->user()->level == 1)
            <li class="{{ request()->is('daftarproduk') ? 'active' : '' }}"><a href="{{ url('/daftarproduk') }}">Daftar Produk</a></li>
            <li class="{{ request()->is('daftartransaksi') ? 'active' : '' }}"><a href="{{ url('/daftartransaksi') }}">Daftar Transaksi</a></li>
        @elseif(auth()->user()->level == 2)
            <li class="{{ request()->is('dafproduk') ? 'active' : '' }}"><a href="{{ url('/dafproduk') }}">Daftar Produk</a></li>
            <li class="{{ request()->is('daftransaksi') ? 'active' : '' }}"><a href="{{ url('/daftransaksi') }}">Daftar Transaksi</a></li>
          @endif
          <li>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="">
              @csrf
              <button type="submit" class="btn btn-primary">Logout</button>
            </form>
          </li>


        </ul>

      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  @yield('isianadmin')

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="container d-md-flex py-4">
      <div class="mr-md-auto text-center text-md-left">
        <div class="copyright">
          &copy; Copyright <strong><span>2021</span></strong>. All Rights Reserved
        </div>
        <div class="credits">
          <!-- All the links in the footer should remain intact. -->
          <!-- You can delete the links only if you purchased the pro version. -->
          <!-- Licensing information: https://bootstrapmade.com/license/ -->
          <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/flattern-multipurpose-bootstrap-template/ -->
          {{-- Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a> --}}
        </div>
      </div>
      {{-- <div class="social-links text-center text-md-right pt-3 pt-md-0">
        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
        <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
        <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
      </div> --}}
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

      <!-- modal -->
  <script src="{{ asset('modal') }}/assets/js/jquery.js"></script>
  <script src="{{ asset('modal') }}/assets/js/popper.js"></script>
  <script src="{{ asset('modal') }}/assets/js/bootstrap.js"></script>

  <!-- Vendor JS Files -->
  <script src="{{ asset('desain') }}/assets/vendor/jquery/jquery.min.js"></script>
  <script src="{{ asset('desain') }}/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="{{ asset('desain') }}/assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="{{ asset('desain') }}/assets/vendor/php-email-form/validate.js"></script>
  <script src="{{ asset('desain') }}/assets/vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="{{ asset('desain') }}/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="{{ asset('desain') }}/assets/vendor/venobox/venobox.min.js"></script>
  <script src="{{ asset('desain') }}/assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="{{ asset('desain') }}/assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="{{ asset('desain') }}/assets/vendor/aos/aos.js"></script>



  <!-- Template Main JS File -->
  <script src="{{ asset('desain') }}/assets/js/main.js"></script>

</body>

</html>
