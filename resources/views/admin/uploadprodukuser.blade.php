@extends('admin/template')

@section('isianadmin')

<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Upload Bukti Pembayaran</h2>
          <ol>
            <li><a href="{{ url('/') }}">Home</a></li>
            <li><a href="{{ url('/daftransaksi') }}">Daftar Transaksi</a></li>
            <li>Upload Bukti Pembayaran</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Portfolio Details Section ======= -->
    <section id="portfolio-details" class="portfolio-details">
      <div class="container">
        <div class="row">
            <form action="/dafproduk/update/{{ $produk->id }}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="kode" value="{{ $produk->kode }}">
            <input type="hidden" name="fotolama" value="{{ $produk->bukti }}">
            <input type="hidden" name="status" value="DIPROSES">
                <div class="mb-3">
                    <label for="namaproduk" class="form-label">Total Pembayaran</label>
                    <input type="text" value="{{ $produk->harga }}" name="namaproduk" class="form-control @error('namaproduk') is-invalid @enderror" id="namaproduk" placeholder="Masukan Nama Produk" readonly>
                    @error('namaproduk')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="col-sm-12">
                    <div class="mb-3">
                            <label for="formFile" class="form-label">Upload Bukti Bayar</label>
                            <input value="{{ old('fotoproduk') }}" class="form-control @error('fotoproduk') is-invalid @enderror" name="fotoproduk" type="file" id="formFile">
                            @error('fotoproduk')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Upload</button>
                </div>
            </form>
        </div>
      </div>
    </section><!-- End Portfolio Details Section -->

  </main><!-- End #main -->
<br><br><br><br><br><br><br>

@endsection
