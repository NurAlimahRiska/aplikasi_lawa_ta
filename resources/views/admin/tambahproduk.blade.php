@extends('admin/template')

@section('isianadmin')

<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Tambah Produk</h2>
          <ol>
            <li><a href="{{ url('/') }}">Home</a></li>
            <li><a href="{{ url('/daftarproduk') }}">Daftar Produk</a></li>
            <li>Tambah Produk</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Portfolio Details Section ======= -->
    <section id="portfolio-details" class="portfolio-details">
      <div class="container">
        <div class="row">
            <form action="/daftarproduk/insert" method="post" enctype="multipart/form-data">
            @csrf
                <div class="mb-3">
                    <label for="kodeproduk" class="form-label">Kode Produk</label>
                    <input value="{{ old('kodeproduk') }}" type="text" name="kodeproduk" class="form-control @error('kodeproduk') is-invalid @enderror" id="kodeproduk" placeholder="Masukan Kode Produk">
                    @error('kodeproduk')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="namaproduk" class="form-label">Nama Produk</label>
                    <input type="text" value="{{ old('namaproduk') }}" name="namaproduk" class="form-control @error('namaproduk') is-invalid @enderror" id="namaproduk" placeholder="Masukan Nama Produk">
                    @error('namaproduk')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="stokproduk" class="form-label">Stok Produk</label>
                    <input type="number" value="{{ old('stokproduk') }}" name="stokproduk" class="form-control @error('stokproduk') is-invalid @enderror" id="stokproduk" placeholder="Masukan Stok Produk">
                    @error('stokproduk')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="hargaproduk" class="form-label">Harga Produk</label>
                    <input type="text" value="{{ old('hargaproduk') }}" name="hargaproduk" class="form-control @error('hargaproduk') is-invalid @enderror" id="stokproduk" placeholder="Masukan harga Produk">
                    @error('hargaproduk')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="formFile" class="form-label">Foto Awal Produk</label>
                    <input value="{{ old('fotoproduk') }}" class="form-control @error('fotoproduk') is-invalid @enderror" name="fotoproduk" type="file" id="formFile">
                    @error('fotoproduk')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="ketproduk" class="form-label">Keterangan Produk</label>
                    <textarea name="ketproduk" class="form-control @error('ketproduk') is-invalid @enderror" id="ketproduk" rows="3">{{ old('ketproduk') }}</textarea>
                    @error('ketproduk')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Simpan</button>
                </div>
            </form>
        </div>
      </div>
    </section><!-- End Portfolio Details Section -->

  </main><!-- End #main -->
<br><br><br><br><br><br><br>

@endsection
