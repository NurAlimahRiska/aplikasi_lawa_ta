@extends('admin/template')

@section('isianadmin')

<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Detail Produk</h2>
          <ol>
            <li><a href="{{ url('/') }}">Home</a></li>
            <li><a href="{{ url('/daftarproduk') }}">Daftar Produk</a></li>
            <li>Detail Produk</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Portfolio Details Section ======= -->
    <section id="portfolio-details" class="portfolio-details">
      <div class="container">
        <h2 class="portfolio-title">{{ $produk->nama }}</h2>
        <div class="row">

          <div class="col-lg-8" data-aos="fade-right">
            <div class="owl-carousel portfolio-details-carousel">
              <img src="{{ asset('gambar/'.$produk->foto)}}" class="img-fluid" alt="">
            </div>
          </div>

          <div class="col-lg-4 portfolio-info" data-aos="fade-left">
            <h3>Infromasi Produk</h3>
            <ul>
              <li><strong>Nama Produk</strong>: {{$produk->nama}}</li>
              <li><strong>Kode Barang</strong>: {{$produk->kode}}</li>

              <!-- <li><strong>Project URL</strong>: <a href="#">www.example.com</a></li> -->
            </ul>

            <p>
            {{$produk->ket}}
            </p>
            <a href="/daftarproduk" class="btn btn-primary">Kembali</a>&nbsp;&nbsp;&nbsp;
            <a href="" class="btn btn-warning">Edit</a>&nbsp;&nbsp;&nbsp;
            <a href="" class="btn btn-danger">Hapus</a>&nbsp;&nbsp;&nbsp;
          </div>

        </div>

      </div>
    </section><!-- End Portfolio Details Section -->

  </main><!-- End #main -->
<br><br><br><br><br><br><br>

@endsection
