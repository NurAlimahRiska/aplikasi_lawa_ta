@extends('admin/template')

@section('isianadmin')

<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Status Transaksi</h2>
          <ol>
            <li><a href="{{ url('/') }}">Home</a></li>
            <li><a href="{{ url('/daftartransaksi') }}">Daftar Transaksi</a></li>
            <li>Perubahan Status</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Portfolio Details Section ======= -->
    <section id="portfolio-details" class="portfolio-details">
      <div class="container">
        <div class="row">
            <form action="/daftartransaksi/statusup/{{ $produk->id }}" method="post">
            @csrf
                <div class="mb-3">
                    <label for="namaproduk" class="form-label">Nama Produk</label>
                    <input type="text" value="{{ $produk->nama }}" name="namaproduk" class="form-control" id="namaproduk" readonly>
                </div>
                <div class="mb-3">
                    <label for="hargaproduk" class="form-label">Harga Produk</label>
                    <input type="text" value="{{ $produk->harga }}" name="hargaproduk" class="form-control" id="hargaproduk" readonly>
                </div>
                <div class="mb-3">
                    <label for="fotoproduk" class="form-label">Produk</label>
                    <input type="hidden" value="{{ $produk->foto }}" name="fotoproduk" id="fotoproduk">
                    <img src="{{ asset('gambar') }}/{{ $produk->foto }}" width="10%" alt="">
                </div>
                <div class="mb-3">
                    <label for="hargaproduk" class="form-label">Total Transaksi</label>
                    <input type="text" value="{{ $produk->total }}" name="hargaproduk" class="form-control" id="hargaproduk" readonly>
                </div>
                <div class="mb-3">
                    <label for="status" class="form-label">Status Transaksi</label>
                    <select name="status" id="status" class="form-control">
                       <option value="{{ $produk->ket }}">{{ $produk->ket }}</option>
                       <option value="Belum Upload Bukti Bayar">Belum Upload Bukti Bayar</option>
                       <option value="DIPROSES">DIPROSES</option>
                       <option value="DIKIRIM">DIKIRIM</option>
                       <option value="SELESEI">SELESEI</option>
                    </select>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">UPDATE</button>
                </div>
            </form>
        </div>
      </div>
    </section><!-- End Portfolio Details Section -->

  </main><!-- End #main -->
<br><br><br><br><br><br><br>

@endsection
