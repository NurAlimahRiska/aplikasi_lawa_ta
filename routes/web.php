<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BerandaController;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\ProdukUserController;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [BerandaController::class, 'index']);
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/tentang', [BerandaController::class, 'tentang']);

//hak akses untuk admin
Route::group(['middleware' => 'admin'],function (){
    //penanganan Produk
    Route::get('/daftarproduk', [ProdukController::class, 'index'])->name('produknya');
    Route::get('/daftarproduk/detail/{id_produk}', [ProdukController::class, 'detail']);
    Route::get('/daftarproduk/tambah', [ProdukController::class, 'tambah']);
    Route::post('/daftarproduk/insert', [ProdukController::class, 'insert']);
    Route::get('/daftarproduk/edit/{idproduk}', [ProdukController::class, 'edit']);
    Route::post('/daftarproduk/update/{idproduk}', [ProdukController::class, 'update']);
    Route::get('/daftarproduk/delete/{idproduk}', [ProdukController::class, 'delete']);
    //area transaksi
    Route::get('/daftartransaksi', [ProdukController::class, 'daftartransaksi'])->name('produknya');
    Route::get('/daftartransaksi/detail/{id_produk}', [ProdukController::class, 'detailtransaksi']);
    Route::get('/daftartransaksi/status/{id_produk}', [ProdukController::class, 'status']);
    Route::post('/daftartransaksi/statusup/{id_produk}', [ProdukController::class, 'statusup']);
});

//akses untuk user
Route::group(['middleware' => 'user'],function (){
    //area user
    //area produk
    Route::get('/dafproduk', [ProdukUserController::class, 'index'])->name('produknya');
    Route::get('/dafproduk/detail/{id_produk}', [ProdukUserController::class, 'detail']);
    Route::get('/dafproduk/beli/{id_produk}', [ProdukUserController::class, 'beli']);
    Route::post('/dafproduk/insert', [ProdukUserController::class, 'insert']);
    //area transaksi
    Route::get('/daftransaksi', [ProdukUserController::class, 'transaksi'])->name('produknya');
    Route::get('/daftransaksi/detail/{id_produk}', [ProdukUserController::class, 'detailtransaksi']);
    Route::get('/daftransaksi/delete/{idproduk}', [ProdukUserController::class, 'delete']);
    Route::get('/daftransaksi/upload/{id_produk}', [ProdukUserController::class, 'uploadtransaksi']);
    Route::post('/dafproduk/update/{id_produk}', [ProdukUserController::class, 'masukbukti']);
});
