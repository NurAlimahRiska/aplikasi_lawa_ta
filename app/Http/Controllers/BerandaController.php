<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BerandaModel;

class BerandaController extends Controller
{
    public function __construct(){
        $this->BerandaModel = new BerandaModel();
        $this->middleware('auth');
    }
    public function index(){
        $data = [
            'isikonten' => $this->BerandaModel->detailDataBeranda(),
        ];
        return view('admin.beranda',$data);
    }
    public function tentang(){
        $data = [
            'isikonten' => $this->BerandaModel->detailDataTentang(),
        ];
        return view('admin.tentang',$data);
    } 
}
