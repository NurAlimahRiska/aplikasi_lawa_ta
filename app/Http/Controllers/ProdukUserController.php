<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProdukUserModel;

class ProdukUserController extends Controller
{
    public function __construct(){
        $this->ProdukUserModel = new ProdukUserModel();
        $this->middleware('auth');
    }
    public function index(){
        $data = [
            'produk' => $this->ProdukUserModel->allData(),
        ];
        return view('admin.daftarprodukuser', $data);
    }
    public function detail($id_produk){
        if(!$this->ProdukUserModel->detailData($id_produk)){
            abort(404);
        }
        $data = [
            'produk' => $this->ProdukUserModel->detailData($id_produk),
        ];
        return view('admin.detailprodukuser', $data);
    }
    public function beli($id_produk){
        $data = [
            'produk' => $this->ProdukUserModel->detailData($id_produk),
        ];
        return view('admin.beliprodukuser', $data);
    }
    public function transaksi(){
        $data = [
            'produk' => $this->ProdukUserModel->transaksiData(),
        ];
        return view('admin.transaksiprodukuser', $data);
    }
    public function insert(){
        Request()->validate([
            'namapenerima' => 'required',
            'telppenerima' => 'required|numeric',
            'alamatpenerima' => 'required',
        ],[
            'namapenerima.required' => 'Penerima Tidak Boleh Kosong',
            'telppenerima.required' => 'Telp Tidak Boleh Kosong',
            'telppenerima.numeric' => 'No Telp berupa angka tanpa titik atau spasi',
        ]);
            $totalnya = Request()->hargaproduk * Request()->jumlahproduk;

            $data = [
                'kode' => Request()->kodeproduk,
                'nama' => Request()->namaproduk,
                'foto' => Request()->fotoproduk,
                'jumlah' => Request()->jumlahproduk,
                'harga' => Request()->hargaproduk,
                'namapenerima' => Request()->namapenerima,
                'telppenerima' => Request()->telppenerima,
                'alamatpenerima' => Request()->alamatpenerima,
                'bukti' => '',
                'ket' => 'Belum Upload Bukti Bayar',
                'total' => $totalnya,
                'tanggal' => Request()->tanggalsekarang,
                'usr' => auth()->user()->id,
            ];
            $this->ProdukUserModel->addProduk($data);
            return redirect('dafproduk')->with('pesansistem','Pesanan Di Buat silahkah ke menu daftar transaksi untuk melanjutkan');
    }
    public function delete($idproduk){
        //hapus record di database
        $this->ProdukUserModel->hapusProduk($idproduk);
        return redirect('daftransaksi')->with('pesansistem','Transaksi Di Hapus atau Dibatalkan');
    }
    public function detailtransaksi($id_produk){
        if(!$this->ProdukUserModel->transaksiData($id_produk)){
            abort(404);
        }
        $data = [
            'produk' => $this->ProdukUserModel->detailtransaksiData($id_produk),
        ];
        return view('admin.detailtransaksiuser', $data);
    }
    public function uploadtransaksi($id_produk){
        if(!$this->ProdukUserModel->detailtransaksiData($id_produk)){
            abort(404);
        }
        $data = [
            'produk' => $this->ProdukUserModel->detailtransaksiData($id_produk),
        ];
        return view('admin.uploadprodukuser',$data);
    }
    public function masukbukti($id_produk){
        Request()->validate([
            'fotoproduk' => 'required|mimes:jpg,jpeg,png|max:1024',

        ],[
            'fotoproduk.required' => 'Wajib Diisi',
            'fotoproduk.mimes' => 'File yang diijinkan : jpg,jpeg,png',
            'fotoproduk.max' => 'Max file 1024MB',
        ]);
        //jika validasi selesai
        if(Request()->fotoproduk <> ""){
            //cek file gambar ada atau tidak
            $filelama=public_path('gambar').'/'.Request()->fotolama;
            //jika ada file
            if(file_exists($filelama)){
                //maka hapus file
                @unlink($filelama);
            }
            //upload gambar dulu
            $file = Request()->fotoproduk;
            $fileName = 'BUKTI'.'-'.Request()->kode.'.'.$file->extension();
            $file->move(public_path('gambar'),$fileName);

                $data = [
                    'bukti' => $fileName,
                    'ket' => Request()->status,
                ];
                $this->ProdukUserModel->uploadbuktiProduk($id_produk,$data);
        } else {
                //
        }

            return redirect('daftransaksi')->with('pesansistem','Bukti Pembayaran Berhasil Di Upload. silahkan tunggu konfirmasi admin');
    }
}

