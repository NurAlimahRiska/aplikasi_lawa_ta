<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProdukModel;

class ProdukController extends Controller
{
    public function __construct(){
        $this->ProdukModel = new ProdukModel();
        $this->middleware('auth');
    }
    public function index(){
        $data = [
            'produk' => $this->ProdukModel->allData(),
        ];
        return view('admin.daftarproduk', $data);
    }
    public function detail($id_produk){
        if(!$this->ProdukModel->detailData($id_produk)){
            abort(404);
        }
        $data = [
            'produk' => $this->ProdukModel->detailData($id_produk),
        ];
        return view('admin.detailproduk', $data);
    }
    public function tambah(){
        return view('admin.tambahproduk');
    }
    public function insert(){
        Request()->validate([
            'kodeproduk' => 'required|unique:product,kode|min:3|max:8',
            'namaproduk' => 'required',
            'fotoproduk' => 'required|mimes:jpg,jpeg,png|max:1024',
            'ketproduk' => 'required',
            'hargaproduk' => 'required|numeric',
        ],[
            'kodeproduk.required' => 'Kode Tidak Boleh Kosong',
            'kodeproduk.unique' => 'Kode Sudah ada, pilih yang lain',
            'kodeproduk.min' => 'min 3 digit/karakter',
            'kodeproduk.max' => 'max 8 digit/karakter',
            'namaproduk.required' => 'Nama Tidak Boleh Kosong',
            'fotoproduk.required' => 'foto Tidak Boleh Kosong',
            'fotoproduk.mimes' => 'File yang diijinkan : jpg,jpeg,png',
            'fotoproduk.max' => 'Max file 1024MB',
            'ketproduk.required' => 'Keterangan Tidak Boleh Kosong',
            'hargaproduk.required' => 'Harga Tidak Boleh Kosong',
            'hargaproduk.numeric' => 'Harga berupa angka tanpa titik',
        ]);
        //buat urutan id
        $urutid = ProdukModel::ambilId();
            //foreach($urutid as $urid);
            $idambil = $urutid;
            $idbaru = $idambil + 1;
        //dd($idbaru);
        //jika validasi selesai
        //upload gambar dulu
        $file = Request()->fotoproduk;
        $fileName = Request()->kodeproduk.'-'.$idbaru.'.'.$file->extension();
        $file->move(public_path('gambar'),$fileName);

            $data = [
                'id' => $idbaru,
                'kode' => Request()->kodeproduk,
                'nama' => Request()->namaproduk,
                'ket' => Request()->ketproduk,
                'foto' => $fileName,
                'harga' => Request()->hargaproduk,
            ];
            $this->ProdukModel->addProduk($data);
            return redirect()->route('produknya')->with('pesansistem','Data Produk Ditambah');
    }
    public function edit($idproduk){
        if(!$this->ProdukModel->detailData($idproduk)){
            abort(404);
        }
        $data = [
            'produk' => $this->ProdukModel->detailData($idproduk),
        ];
        return view('admin.editproduk',$data);
    }
    public function update($idproduk){
        Request()->validate([
            'kodeproduk' => 'required|min:3|max:8',
            'namaproduk' => 'required',
            'fotoproduk' => 'mimes:jpg,jpeg,png|max:1024',
            'ketproduk' => 'required',
        ],[
            'namaproduk.required' => 'Nama Tidak Boleh Kosong',
            'fotoproduk.mimes' => 'File yang diijinkan : jpg,jpeg,png',
            'fotoproduk.max' => 'Max file 1024MB',
            'ketproduk.required' => 'Keterangan Tidak Boleh Kosong',
        ]);
        //jika validasi selesai
        if(Request()->fotoproduk <> ""){
            //cek file gambar ada atau tidak
            $filelama=public_path('gambar').'/'.Request()->fotolama;
            //jika ada file
            if(file_exists($filelama)){
                //maka hapus file
                @unlink($filelama);
            }
            //upload gambar dulu
            $file = Request()->fotoproduk;
            $fileName = Request()->kodeproduk.'.'.$file->extension();
            $file->move(public_path('gambar'),$fileName);

                $data = [
                    'kode' => Request()->kodeproduk,
                    'nama' => Request()->namaproduk,
                    'ket' => Request()->ketproduk,
                    'foto' => $fileName,
                ];
                $this->ProdukModel->editProduk($idproduk,$data);
        } else {
                $data = [
                    'kode' => Request()->kodeproduk,
                    'nama' => Request()->namaproduk,
                    'ket' => Request()->ketproduk,
                ];
                $this->ProdukModel->editProduk($idproduk,$data);
        }

            return redirect()->route('produknya')->with('pesansistem','Data Produk Di Perbaharui');
    }

    public function delete($idproduk){
        //hapus gambar
        $produk = $this->ProdukModel->detailData($idproduk);
        if($produk->foto <> ""){
            $lokasi= public_path('gambar').'/'.$produk->foto;
            if(file_exists($lokasi)){
                @unlink($lokasi);
            }
        }
        //hapus record di database
        $this->ProdukModel->hapusProduk($idproduk);
        return redirect()->route('produknya')->with('pesansistem','Data Produk Di Hapus');
    }

    //area transaksi
    public function daftartransaksi(){
        $data = [
            'produk' => $this->ProdukModel->transaksiData(),
        ];
        return view('admin.daftartransaksi', $data);
    }
    public function detailtransaksi($id_produk){
        if(!$this->ProdukModel->transaksiData($id_produk)){
            abort(404);
        }
        $data = [
            'produk' => $this->ProdukModel->detailtransaksiData($id_produk),
        ];
        return view('admin.detailtransaksi', $data);
    }
    public function status($id_produk){
        if(!$this->ProdukModel->transaksiData($id_produk)){
            abort(404);
        }
        $data = [
            'produk' => $this->ProdukModel->detailtransaksiData($id_produk),
        ];
        return view('admin.statustransaksi', $data);
    }
    public function statusup($id_produk){
        $data = [
            'ket' => Request()->status,
        ];
        $this->ProdukModel->statusProduk($id_produk,$data);
        return redirect('daftartransaksi')->with('pesansistem','Status Transaksi Di Perbaharui');
    }

}
