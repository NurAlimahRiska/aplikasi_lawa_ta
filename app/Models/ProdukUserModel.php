<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProdukUserModel extends Model
{
    public function allData(){
        return DB::table('product')->get();
    }
    public function detailData($id_produk){
        return DB::table('product')->where('id', $id_produk)->first();
    }
    public function addProduk($data){
        DB::table('transaksi')->insert($data);
    }
    public function transaksiData(){
        return DB::table('transaksi')->get();
    }
    public function hapusProduk($idproduk){
        DB::table('transaksi')->where('id',$idproduk)->delete();
    }
    public function detailtransaksiData($id_produk){
        return DB::table('transaksi')->where('id', $id_produk)->first();
    }
    public function uploadbuktiProduk($id_produk,$data){
        DB::table('transaksi')->where('id',$id_produk)->update($data);
    }
}
