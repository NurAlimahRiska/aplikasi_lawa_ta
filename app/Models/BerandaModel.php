<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class BerandaModel extends Model
{
    public function detailDataBeranda(){
        return DB::table('konten')->where('id', '1')->first();
    }
    public function detailDataTentang(){
        return DB::table('konten')->where('id', '2')->first();
    }
}
