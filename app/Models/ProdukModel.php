<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProdukModel extends Model
{
    public function allData(){
        return DB::table('product')->get();
    }
    public function detailData($id_produk){
        return DB::table('product')->where('id', $id_produk)->first();
    }
    public function addProduk($data){
        DB::table('product')->insert($data);
    }
    public function editProduk($idproduk,$data){
        DB::table('product')->where('id',$idproduk)->update($data);
    }
    public static function ambilId(){
        return $ambilId = DB::table('product')->count();
    }
    public function hapusProduk($idproduk){
        DB::table('product')->where('id',$idproduk)->delete();
    }
    //area untuk transaksi
    public function transaksiData(){
        return DB::table('transaksi')->get();
    }
    public function detailtransaksiData($id_produk){
        return DB::table('transaksi')->where('id', $id_produk)->first();
    }
    public function statusProduk($id_produk,$data){
        DB::table('transaksi')->where('id',$id_produk)->update($data);
    }
}
